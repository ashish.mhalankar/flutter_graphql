String queryAllGroups = """
  query AllGroups(\$category:String) {
  allGroups(category: \$category) {
    id
    name
  }
}


""";
