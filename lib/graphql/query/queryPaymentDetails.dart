const userInfo = """
query Me {
  me{
     user{
      fullName
      lastName
      number
      address{
        address
        village
      }
    }
  }
}
""";


const customInvoiceDetail = """
query CustomInvoiceDetail(\$orderItems:[OrderItemStockInput]!){
    customInvoiceDetail(
        orderItems: \$orderItems) {
        id
        trackingCode
        pocName
        pocNumber
        invoiceItems {
            name
            quantity
            actualRate
            discountedRate
            category
            itemType
            amount
            balance
        }
        discountCode
        discountPercentage
        deliveryAddress {
            village
            address
        }
        availablePaymentModes
    }
}
""";



