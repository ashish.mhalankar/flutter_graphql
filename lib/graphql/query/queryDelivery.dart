String queryDeliveryCurrentTrip = """
  query DeliveryCurrentTrips(\$date_Gte: Date, \$date_Lte: Date,\$status:[String]) {
    allAggregatorTrips(filter: {status: \$status, date_Gte: \$date_Gte, date_Lte: \$date_Lte}) {
        id
        uuid
        date
        estimatedStartTime
        status
        sourceDepot {
            name
            location {
                latitude
                longitude
            }
        }
        destinationDepot {
            name
            location {
                latitude
                longitude
            }
        }
        vehicle {
            photo
            registrationNumber
            modelName
            vehicleType {
                name
            }
            driver {
                fullName
                number
            }
        }
    
    }
}

""";

String queryDeliveryTripSteps = """
  query DeliveryTripSteps(\$uuid:String){
    tripStepGraph(uuid: \$uuid) {
    name
    stepId
    timestamp
    bookingObj {
      order {
        id
        trackingCode
        pocName
        pocNumber
        orderStatus
        shippingAddress {
          address
          village
          latitude
          longitude
        }
      }
    }
  }
}
""";
