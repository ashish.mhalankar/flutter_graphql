String queryUsersByGroup = """
  query UsersByGroup(\$query:String) {
  usersByGroup(groupName: "Wholeseller",query:\$query) {
    id
    fullName
    number
    status
    address {
      id
      address
      district
      latitude
      longitude
    }
  }
}

""";

String queryvendorsBySalesman = """
 query vendorsBySalesman(\$query:String, \$status:String,\$salesmanId:ID!) {
  vendorsBySalesman(query: \$query, filter: {status: [\$status], salesmanId: \$salesmanId}) {
    id
    fullName
    number
    status
    groups{
      name
    }
    address {
      id
      address
      district
      latitude
      longitude
    }
    shopSet {
      id
    }
  }
}

""";

String queryAllBusienssCategorys = """
 query queryAllBusienssCategorys {
  allBusienssCategorys {
    id
    name
  }
}


""";

String queryallFamilyGroups = """
query allFamilyGroups{
  allFamilyGroups(query:"OTHER_BUSINESS"){
    id
    name
  }
}
""";

String queryAllFamilyGroupHierarchys = """
query AllFamilyGroupHierarchys {
  allFamilyGroupHierarchys(query: "SHOP_OWNER") {
    id
    name
    codename
  }
}
""";

String queryAllUserOrders = """
 query AllOrders(\$orderStatus:[String],\$userId:ID){
  allOrders(filter:{orderStatus:\$orderStatus,userId:\$userId}) {
    id
    trackingCode
    date
    totalAmount
    orderStatus
    orderItems {
      id
      sellableItem {
        id
        name
      }
    }
  }
}


""";

String queryOrderDetails = """
 query OrderDetails(\$orderId:ID!){
    myOrderDetail(id:\$orderId) {
        id
        deliveryStatus
        orderStatus
        trackingCode
        totalAmount
        totalPaid
        shippingMethod
        paymentMethod
        date
        otp
        pocNumber
        transactions{
            createdAt
            status
            referenceId
        }
        orderItems {
            id
            quantity
            actualRate
            rate
            bookedQuantity
            deliveredQuantity
            shipping{
                estimatedDelivery
            }
            sellableItem {
                name
                shortDescription

                primaryPhoto {
                    url
                }
            }
        }

    }
}


""";

String queryInvoiceOrderDetails = """
 query OrderDetails(\$orderId:ID!){
    myOrderDetail(id: \$orderId) {
        id
        trackingCode
        paymentStatus
        orderStatus
        paymentMethod
        totalPaid
        totalAmount
            user{
                firstName
                number
                email
            }

    }
}


""";

String queryOrderTransactions = """
 query OrderDetails(\$orderId:ID!){
    myOrderDetail(id:\$orderId) {
        transactions{
            createdAt
            status
            referenceId
            amount
             attachment
             {
                  id
                  url
             }
        }
    }
}


""";

String queryDailyMasterOtp = """
 query queryDailyMasterOtp {
  dailyMasterOtp
}
""";
