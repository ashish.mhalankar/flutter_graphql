const selleableItemStockQuery = """

query allMySellableItemStocks(\$query:String,\$latitude:Float,\$longitude:Float,
\$userId:ID,\$rejectedStockCount_Gte:Int)
{
      allMySellableItemStocks(query: \$query,latitude:\$latitude,longitude:\$longitude,
      userId:\$userId,filter: {rejectedStockCount_Gte: \$rejectedStockCount_Gte} ) 
    {
        id
        currentStockCount
        offerStockCount
        holdStockCount
        rejectedStockCount
        discountedRate
        unitRate
        orderingConditions
        minRate
        maxRate
        sellableItem {
            id
            name
            shortDescription
            overallRating
            primaryPhoto {
                url
            }
        }

    }
}



""";

const selleableItemCartQuery = """
query SellableItemCart(\$sellableItemIds:[ID]!){
    sellableItemStockCart(ids: \$sellableItemIds) {
        id
        currentStockCount
        offerStockCount
        holdStockCount
        rejectedStockCount
        discountedRate
        unitRate
        minRate
        maxRate
        orderingConditions
        distributionCentre {
          id
          deliveryConditions
           pickupSlots {
             id
                name
           }
        }
        sellableItem {
            id
            name
            shortDescription
            overallRating
            primaryPhoto{
                url
            }
        }

    }
}
""";
