String deleteBaseUser = """
   mutation deleteBaseUser(\$id: Int!) {
    delBaseUser(userId: \$id) {
      user {
        id
        number
        status
      }
    }
  }
""";

String mutationAddTemporaryOrderHomeDelivery = """
   mutation AddTemporaryOrderNew(\$userId:ID,\$shippingAddress:AddressInput,\$shippingMethod:String!,
   \$orderItems:[OrderItemStockInput]!
\$pocName:String,\$pocNumber:String,\$discountId:ID,\$paymentMethod:String,\$sourceStock:String){
        addTemporaryOrder(
            shippingMethod: \$shippingMethod
            orderItems: \$orderItems
            pocName: \$pocName
            pocNumber: \$pocNumber
            discountId: \$discountId
            paymentMethod:\$paymentMethod
            userId: \$userId,
            shippingAddress: \$shippingAddress
            sourceStock: \$sourceStock


        ) {
            order {
                id
                trackingCode

            }
        }
}
""";

String mutationAddTemporaryOrderPickUP = """
   mutation AddTemporaryOrderNew(\$userId:ID,\$shippingMethod:String!,
   \$orderItems:[OrderItemStockInput]!,
\$pocName:String,\$pocNumber:String,\$discountId:ID,\$paymentMethod:String,\$distributionCentreId:ID!,
\$timeSlotId:ID!,\$sourceStock:String){
        addTemporaryOrder(
            shippingMethod: \$shippingMethod
            orderItems: \$orderItems
            pocName: \$pocName
            pocNumber: \$pocNumber
            discountId: \$discountId
            paymentMethod:\$paymentMethod
            userId: \$userId,
            sourceStock: \$sourceStock,
            pickupSlots:{distributionCentreId: \$distributionCentreId, timeSlotId: \$timeSlotId}

        ) {
            order {
                id
                trackingCode

            }
        }
}
""";
String mutationDeliverOrder = """
  mutation DeliverOrder(\$orderId:ID!,\$latitude:Float!,\$longitude:Float!,\$orderStatus:String!,\$deliveryStatus:String!,\$remark:String,
    \$orderItems:[OrderItemInput]){
    deliverOrder(
        id: \$orderId
        latitude:\$latitude
        longitude: \$longitude
        remark: \$remark
        orderItems:\$orderItems
        deliveryStatus: \$deliveryStatus
        orderStatus: \$orderStatus
    ) {
        order {
            id
            totalPaid
            totalAmount
            paymentMethod
            orderStatus
        }
    }
}

""";

String mutationAddUpdateOrderTransaction = """
  mutation AddUpdateOrderTransaction(\$orderId:ID!\$amount:Float!,\$status:String!,\$orderStatus:String,\$paymentMethod:String,
  \$referenceId:String,\$attachment:MediaInput){
    addUpdateOrderTransaction(
        orderId: \$orderId
        amount: \$amount
        status: \$status
        orderStatus: \$orderStatus
        referenceId: \$referenceId
        paymentMethod: \$paymentMethod,
        attachment:\$attachment

    ) {
        orderTransaction {
            id
            referenceId
            status
        }
    }
}

""";

String mutationAddUpdateVendor = """
  mutation
  addUpdateVendor(\$firstName:String!,\$lastName:String!,\$number:String!,\$email:String,
  \$dateOfBirth:Date,\$username:String!,\$address: AddressInput!,\$groups:[ID],
  \$education:BaseUserEducationEnum,\$gender:BaseUserGenderEnum!,){
    addUpdateVendor(
    firstName: \$firstName
    lastName: \$lastName
    number: \$number
    email: \$email
    dateOfBirth: \$dateOfBirth
    username: \$username
    address: \$address
    groups: \$groups
    education: \$education
    gender: \$gender
  ) {
    user {
      id
    }
  }
}

""";

String mutationAddUpdateShop = """
 mutation addUpdateShop(\$id:ID,\$familyGroupId:ID,\$name:String,\$category:String,\$address:AddressInput!){
    addUpdateShop(
        id:\$id
        familyGroupId:\$familyGroupId,
        name:\$name,
        category:\$category,
        address:\$address,
    ){
        shop{
            id
            address{
                id
            }
        }

    }
}

""";

String mutationAddUpdateShopPointsOfContacts = """
 mutation
AddUpdateShopPointsOfContacts(\$shopId:ID!,\$salesmanId:ID,\$username:String,\$number:String!,\$firstName:String,\$lastname:String,\$email:String,
\$familyGroupId:ID,\$familyGroupHierarchyId:ID,\$address:AddressInput!,\$groupIds:[Int],
){
    addUpdateShopPointsOfContacts(
    shopId:\$shopId,
    salesmanId: \$salesmanId
        pointOfContacts:
        {
            username:\$username,
            number:\$number,
            firstName:\$firstName,
            lastName:\$lastname,
            email:\$email,
            familyGroupId:\$familyGroupId,
            familyGroupHierarchyId:\$familyGroupHierarchyId,
            address:\$address
            groupIds:\$groupIds

        }
    ){
        pointOfContacts{
            id
            address{
                id
            }
        }
    }
}
""";
