import 'package:graphql_flutter/graphql_flutter.dart';

class MutationHelper {
  static HttpLink link(String token) {
    return HttpLink(uri: "https://herd.vesatogo.com/api/v3/", headers: {
      "Authorization": token != null ? "JWT " + token : "",
      "Tenant": "sahyadrifarms",
    });
  }

  static uri() {
    return Uri.parse("https://herd.vesatogo.com/services/uploader/");
  }

  static headers(String token) {
    Map<String, String> headers = {
      "Authorization": token != null ? "JWT " + token : "",
      "Tenant": "sahyadrifarms",
    };
    return headers;
  }

  static Map<String, dynamic> addressInput(String id, String address,
      String district, double latitude, double longitude) {
    Map<String, dynamic> input = {};
    if (id != null) {
      input.putIfAbsent('id', () => id);
    }
    if (address != null) {
      input.putIfAbsent('address', () => address);
    }
    if (district != null) {
      input.putIfAbsent('district', () => district);
    }
    if (latitude != null) {
      input.putIfAbsent('latitude', () => latitude);
    }
    if (longitude != null) {
      input.putIfAbsent('longitude', () => longitude);
    }
    print("addressInput " + input.toString());

    return input;
  }
}
