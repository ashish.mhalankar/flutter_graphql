import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class HelpterFuncation {
  static error(BuildContext context, msg) {
    Toast.show(msg, context, duration: Toast.LENGTH_LONG, gravity: Toast.TOP);
  }
}
