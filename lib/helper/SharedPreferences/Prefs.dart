import 'package:fleet_flutter/helper/IConstant.dart';

import './PreferencesHelper.dart';
import './PrefsConst.dart';

class Prefs {
  static Future<String> get getToken =>
      PreferencesHelper.getString(IConstanct.session);

  static Future setToken(String value) =>
      PreferencesHelper.setString(PrefsConst.token, value);

  static Future<bool> get getAuthenticated =>
      PreferencesHelper.getBool(PrefsConst.session);

  static Future<String> get getUserId =>
      PreferencesHelper.getString(IConstanct.userId);

  static Future setAuthenticated(bool value) =>
      PreferencesHelper.setBool(PrefsConst.session, value);
  static Future<String> get getDepartmentId =>
      PreferencesHelper.getString(PrefsConst.demartmentId);

  static Future setDepartmentId(String value) =>
      PreferencesHelper.setString(PrefsConst.demartmentId, value);

  Future<void> clear() async {
    await Future.wait(<Future>[setAuthenticated(false), setToken('')]);
  }
}
