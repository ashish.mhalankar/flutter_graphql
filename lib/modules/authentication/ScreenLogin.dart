import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:fleet_flutter/graphql/graphql-configuration.dart';
import 'package:fleet_flutter/graphql/mutation/mutationLogin.dart';
import 'package:fleet_flutter/helper/IConstant.dart';
import 'package:fleet_flutter/main.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

GraphQLConfiguration graphQLConfiguration = GraphQLConfiguration();

class ScreenLogin extends StatefulWidget {
  static const routeName = '/ScreenLogin';
  bool obscureText = true;
  Function(String) userToken;
  ScreenLogin({this.userToken});

  @override
  _ScreenLoginState createState() => _ScreenLoginState();
}

class _ScreenLoginState extends State<ScreenLogin> {
  var _inputTextMobileNo = new TextEditingController();

  var _inputPassword = new TextEditingController();

  final RoundedLoadingButtonController _btnController =
      new RoundedLoadingButtonController();

  final _formKey = GlobalKey<FormState>();

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  String firebaseToken;

  _register() {
    _firebaseMessaging.getToken().then((token) {
      print("Firebase token " + token);
      setState(() {
        firebaseToken = token;
      });
    });
  }

  setLogin(String token, String departmentId, String userId,
      BuildContext ctx) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(IConstanct.session, true);
    prefs.setString(IConstanct.token, token);
    prefs.setString(IConstanct.departmentId, departmentId);
    prefs.setString(IConstanct.userId, userId);

    widget.userToken(token);

    _btnController.success();

    graphQLConfiguration.clientToQuery();

    Navigator.pushNamedAndRemoveUntil(ctx, MyApp.routeName, (r) => false);
  }

  @override
  void initState() {
    super.initState();

    _register();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _formKey,
        child: Center(
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.all(20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: 100,
                    width: 100,
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.2),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: Offset(0, 2), // changes position of shadow
                          ),
                        ],
                        borderRadius: BorderRadius.all(Radius.circular(100)),
                        border: Border.all(
                            color: Colors.green, style: BorderStyle.solid)),
                    child: Image.asset(
                      'asset/images/ic_logo.png',
                      height: 100,
                    ),
                  ),
                  TextFormField(
                    onTap: () {},
                    keyboardType: TextInputType.phone,
                    textInputAction: TextInputAction.next,
                    maxLength: 10,
                    controller: _inputTextMobileNo,
                    // onSubmitted: (_) => FocusScope.of(context).nextFocus(),
                    validator: (value) {
                      if (value.length != 10) {
                        return 'Please enter correct Mobile Number';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      labelText: 'Mobile Number',
                      icon: Icon(Icons.phone_android),
                    ),
                  ),
                  TextFormField(
                      obscureText: widget.obscureText,
                      keyboardType: TextInputType.visiblePassword,
                      textInputAction: TextInputAction.next,
                      controller: _inputPassword,
                      //onSubmitted: (_) => FocusScope.of(context).nextFocus(),
                      decoration: InputDecoration(
                          labelText: 'Password',
                          icon: Icon(Icons.lock_open),
                          suffixIcon: InkWell(
                              onTap: () {
                                setState(() {
                                  if (widget.obscureText) {
                                    widget.obscureText = false;
                                  } else if (!widget.obscureText) {
                                    widget.obscureText = true;
                                  }
                                });
                              },
                              child: Icon(widget.obscureText
                                  ? Icons.visibility
                                  : Icons.visibility_off))),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter correct Password';
                        }

                        return null;
                      }),
                  SizedBox(
                    height: 20,
                  ),
                  Mutation(
                    options: MutationOptions(
                      documentNode: gql(login),
                      update: (Cache cache, QueryResult result) {
                        return cache;
                      },
                      onCompleted: (dynamic result) {
                        print(result);
                        if (result != null) {
                          Map<String, dynamic> resultData = result;

                          setLogin(
                              resultData['login']['token'],
                              resultData['login']['me']['currentDepartment'] !=
                                      null
                                  ? resultData['login']['me']
                                      ['currentDepartment']['id']
                                  : "",
                              resultData['login']['me']['id'],
                              context);
                        }
                      },
                      onError: (error) {
                        _btnController.error();
                        _btnController.reset();

                        Toast.show("Enter Username or Password", context,
                            duration: Toast.LENGTH_LONG, gravity: Toast.TOP);
                      },
                    ),
                    builder: (
                      RunMutation runMutation,
                      QueryResult result,
                    ) {
                      return Container(
                        margin: EdgeInsets.only(left: 30, right: 30, top: 10),
                        child: RoundedLoadingButton(
                          controller: _btnController,
                          onPressed: () {
                            if (_formKey.currentState.validate()) {
                              Map<String, dynamic> data = {
                                'username': _inputTextMobileNo.text,
                                'password': _inputPassword.text,
                                'firebaseToken': firebaseToken
                              };
                              print(data);
                              runMutation(data);
                            } else {
                              _btnController.reset();
                            }
                          },
                          child: Text('Sign in',
                              style: TextStyle(color: Colors.white)),
                          color: Theme.of(context).primaryColor,
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
